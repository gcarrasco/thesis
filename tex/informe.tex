\documentclass[11pt,a4paper,oneside]{scrartcl}
\usepackage[english]{babel}
\usepackage[pdftex]{graphicx}
\usepackage[export]{adjustbox}

\usepackage{comment}

\tolerance=1
\emergencystretch=\maxdimen
\hyphenpenalty=10000
\hbadness=10000

\title{Final project - Midterm report of: Automating installation, 
       testing and development of bcbio-nextgen pipeline}
\author{Guillermo Carrasco}

\begin{document}
\pagenumbering{arabic}
\maketitle

\section{Objectives of this project}
The necessity to understand the DNA structure has driven the development of 
Bioinformatics or Computational biology. Nowadays, week-long sequencing runs on a
single machine can produce as much data as entire genome centers did a few years ago. 
This translates into an exponential growth of the information available. Thus, the need 
to process terabytes of information has become {\em de rigueur} for many 
laboratories engaged in genomic research. Therefore, fast and scalable 
computing solutions are constantly needed.

Together with the need of fast solutions that deal with huge amount of data, there
is another problem. Sometimes in this kind of scientific environments, the
structure of the software development platform is not very well defined. The merit
of really good algorithms is occasionally overcasted by the difficulty on 
scaling the software, or by a tedious installation procedure of this one. The 
overfitting to an specific platform is also a very common problem in bioinformatic core
facilities.

This final project tries to provide a solution to those problems in a real bioinformatics
core facility in {\em Science For Life Laboratory}. {\em Science For Life Laboratory} 
is a center for large-scale biosciences with the focus in health and environmental research.
It is located in Stockholm, Sweden. Altogether, 15 next generation sequencing instruments
are available at present, with a combined capacity for DNA sequencing equal to 
several hundreds of complete human genomes per year. This implies a massive generation
of data to manage and analyze. Bcbio-nextgen, an in-house developed and maintained
genomics pipeline, is used for this purpose.

The general goals to be accomplished in this project follow:

\begin{enumerate}
    \item Automate the installation procedure of the mentioned pipeline, as it
    was not straightforward at the beginning of this project. Also, automate the 
    testing and deployment of the pipeline using a {\em Continuous Integration System (CI)}, so the
    installation procedure and the future changes in the pipeline can be 
    automatically tested.
    \item Introduce a new parallelization scheme to one of the processes
    of the pipeline using Hadoop. Hadoop is a framework 
    that allows the distributed processing of large data sets across clusters
    of computers using simple programming models. It is designed to scale up from
    single servers to thousands of machines. Each machine offers local computation and
    storage. Thus, the scalability and generality premises established before are
    accomplished. 
\end{enumerate}

\section{Current state}
So far, the first goal of this project has been completed, as well as part of
the second one. In detail:
\begin{enumerate}
    \item The installation of the pipeline has been completely automated.
    \begin{enumerate}
        \item All the software dependencies of the pipeline have been 
        properly packaged. This allows a completely automatic installation, 
        without having to care about installing third party software in advance.
        The packaging has been done following the Debian/Ubuntu packaging guide. 
        This process ensures a more general and portable solution, as all dependencies
        can be installed in any Debian based system simply including the public 
        software repository created for this purpose.
        \item A system to completely automate the installation of the pipeline has been
        developed. It has two main modes of installation:
            \begin{enumerate}
                \item {\bf Virtual machine} installation: In this mode, the pipeline
                is installed in one or several virtual machines created by the 
                system itself.
                \item {\bf Local } installation in the machine: In this mode the
                pipeline is fully installed in the current machine. 
            \end{enumerate}
    \end{enumerate}
    In order to test the installation procedure, and also to automate the
    testing of future changes such as the Hadoop integration, the pipeline
    has been adapted to work within a {\em CI} system.
    This adaptation allows us to automatically test {\bf any} change made in the
    pipeline code just committing to GitHub \footnote{GitHub is a web-based hosting 
    service for projects that use the Git revision control system}. Thus we can
    easily track the current state of the code, when and why it failed, etc.
    \item Regarding to the Hadoop improvement, I have created and tested the
    environment where the tests will be run. This environment has been created
    having in mind the reusability and reproducibility principles:
    \begin{enumerate}
        \item The environment consists on four virtual machines running Ubuntu
        distribution.
        \item One machine has been configured as a Hadoop namenode \footnote{The
        NameNode is the centerpiece of a Hadoop File System. It keeps the directory
        tree of all files in the file system, and tracks where across the cluster
        the file data is kept. It does not store the data of these files itself.}.
        The rest of them have the role of datanodes \footnote{A DataNode stores data
        in the Hadoop File System. A functional filesystem has more than one 
        DataNode, with data replicated across them.}.
    \end{enumerate}
    It is important to notice that the {\bf full} testing environment can be
    easily reproduced thanks to the software provisioners used to configure it.
    Besides the Ubuntu software packages, the Hadoop roles and configuration files
    are automatically set using a Chef \footnote{Chef solo is a software
    provisioner, pleas refer to http://wiki.opscode.com/ to more information} 
    recipe.
\end{enumerate}

\section{Planning of the remaining work}
The remaining work for this project consists on adapting a part of the analysis
performed by this pipeline so it uses Hadoop to improve the performance
and scalability.

To do so, I have decided to use Seal. Seal is a suite of distributed applications
for aligning short DNA reads. Seal applications generally run on the Hadoop framework.
This adaptation will help to corroborate that the previous work done in this project
is working properly, since the pipeline, as well as Hadoop, have to be installed,
configured and tested in several machines.

It will also let us know if it is worthy to use a parallel framework such as 
MapReduce in a genomics pipeline like ours.

Figure \ref{fig:gantt} shows the gantt chart for the remaining work.

\begin{figure}[htb]
    \begin{center}
        \includegraphics[width=550bp, height=150bp, rotate=90]{images/ganttCurrent.png}
        \caption{Gantt chart for this project. Black line on each task represents
        the completed percentage of the task. Current state represented by a vertical green line.} \label{fig:gantt}
    \end{center}
\end{figure}

\end{document}