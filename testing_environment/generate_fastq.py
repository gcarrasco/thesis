#Source: https://github.com/SciLifeLab/facs/blob/master/tests/utils/helpers.py
import sys
import os

header='@HWI-ST188:2:1101:2751:1987#0/1'
ecoli_read = \
"""
AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGCTTCTGAACTGGTTACCTGCCGTGAGTAAATTAAA
+ 
#!#$+*"\'%&,+)"\'\'\'+%!*\'+##)!(#&(,,%!%!#$%#\')!!,%+!%(#"!(&),%)#""!"$\'%%**&&+&%,&,*%+#)%!$*$,"(#()*$\'&%+$)!
"""

def _generate_dummy_fastq(fname, size):
    """ Generates simplest reads with dummy qualities
    """
    stride=13

    if not os.path.exists(fname):
        with open(fname, "w") as f:
            f.write(header)
            # Spike one ecoli read
            f.write(ecoli_read)
            read = 0

            while os.path.getsize(fname) <= size*1024.*1024.:
                # Identify reads uniquely for later debugging (task distribution, for instance)
                f.write(header + 'TASK ID: ' + str(read) + '\n')
                 
                f.write('GATTACAT' * stride + '\n')
                f.write('+' + '\n')
                f.write('arvestad' * stride + '\n')
                read += 1

try:
	size = sys.argv[1]
except IndexError:
	sys.exit("Usage: generate_fastq.py size_in_MB")
print "Generating fasq file with size = " + size + " MB..."
_generate_dummy_fastq('test.fastq', int(size))
print "DONE!"