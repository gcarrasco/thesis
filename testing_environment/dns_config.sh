#!/bin/bash

#DNS configuration
if [ `hostname` == "dns" ]
then
    apt-get install -y dnsmasq
    echo "10.10.10.3    namenode" >> /etc/hosts
    echo "10.10.10.4    datanode1" >> /etc/hosts
    echo "10.10.10.5    datanode2" >> /etc/hosts
    echo "10.10.10.6    datanode3" >> /etc/hosts
    /etc/init.d/dnsmasq restart
else
    echo "nameserver 10.10.10.2" >> /etc/resolvconf/resolv.conf.d/head
    /etc/init.d/networking restart
fi
