#!/bin/bash

IDS_FILE=datanodes_id.txt
N=$1

if [ "$N" == "kill" ]
then
    for id in `cat ${IDS_FILE}`
    do
        onevm delete $id && echo "Deleting datanode ${id}"
    done
    rm ${IDS_FILE}
elif [ "$N" != "" ]
then
    #Remove previous id list
    rm ${IDS_FILE}

    #Create a bunch of datanodes and store its IDs for future removal
    for ((a=1; a<=${N}; a++))
    do
        IFS=' ' read -ra ID <<< `onevm create /opt/templates/ubuntu.vmdef` && echo "${ID[1]}" >> ${IDS_FILE}
    done
else
    echo "Usage ./start_datanodes.sh [N | kill]"
fi