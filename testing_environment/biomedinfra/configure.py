from __future__ import with_statement
from fabric.api import *
from fabric.operations import sudo

env.hosts = ['10.25.1.4']
env.user = 'ubuntu'
env.password = 'ubuntu'

def _add_hostname_to_dns():
    """Add hostname and IP to the DNS.

    Captures the IP address and the hostname and appends them to the /etc/hosts
    file in the namenode
    """
    
    hostsfile = '/etc/hosts'
    hostname = local('hostname', capture=True)
    ip_address = local("/sbin/ifconfig eth0 | grep \"inet addr\" | awk -F: '{print $2}' | awk '{print $1}'", capture=True)
    cmd = "echo -e \"{}{}{}\" >> {}".format(ip_address, '\t', hostname, hostsfile)
    sudo(cmd)

def configure():
    _add_hostname_to_dns()