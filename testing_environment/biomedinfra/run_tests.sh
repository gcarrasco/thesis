#!/bin/bash

# Run tests for the PFC. In each call, it will:
#    - Copy the test data to HDFS
#    - Execute seal_prq to convert the fastq files
#    - For each dataset (2 per genome):
#        + Execute seal_seqal alignment. Each alignment will be executed several times. 
#          The first time is expected to be the slowest due to HDFS transferring
#          the data to all datanodes.
#        + Execute bwa in multicore mode
#
# The script will time each execution and save the results in a unique file

#Data locations and destinies
NFS=/mnt/stu/guillem_thesis
TEST_DATA_DIR=${NFS}/test_data
ECOLI_DATA=${TEST_DATA_DIR}/ecoli
HG19_DATA=${TEST_DATA_DIR}/hg19
DM3_DATA=${TEST_DATA_DIR}/dm3
REF_ECOLI_SEQAL=references/ecoli/ecoli.tar
REF_ECOLI_BWA=${TEST_DATA_DIR}/reference_genomes_bwa/ecoli/eschColi_K12.fa
REF_HG19_SEQAL=references/hg19/hg19.tar
REF_HG19_BWA=${TEST_DATA_DIR}/reference_genomes_bwa/hg19/hg19.fa
REF_DM3_SEQAL=references/dm3/dm3.tar
REF_DM3_BWA=${TEST_DATA_DIR}/reference_genomes_bwa/dm3/dm3.fa
RESULTS_DIR=${NFS}/results
RESULTS_SEAL=${RESULTS_DIR}/seal
RESULTS_SEAL_ECOLI=${RESULTS_SEAL}/ecoli
RESULTS_SEAL_HG19=${RESULTS_SEAL}/hg19
RESULTS_SEAL_DM3=${RESULTS_SEAL}/dm3
RESULTS_BWA=${RESULTS_DIR}/bwa-0.5.9
RESULTS_BWA_ECOLI=${RESULTS_BWA}/ecoli
RESULTS_BWA_HG19=${RESULTS_BWA}/hg19
RESULTS_BWA_DM3=${RESULTS_BWA}/dm3
RESULTS_BWA_ALIGNMENTS=${RESULTS_BWA}/alignments

#Script variables
EXEC_TIMES=3
CPUS=`cat /proc/cpuinfo | grep processor | wc -l`
MAILTO="guillermo.carrasco@scilifelab.se roman.valls.guimera@scilifelab.se"


# Function to 'tee' echo to the log
techo() {
    echo "$1" | tee -a tests.log
}


function timeit() {
    mkdir -p $2
    case "$1" in
        "transfer")
            /usr/bin/time -a -o $2/transfer_time.out hadoop fs -put $3
            shift;;

        "prq")
            /usr/bin/time -a -o $2/prq_conversion_time.out seal_prq --input-format=fastq $3 $4 &> $2/prq_conversion.out
            shift;;

        "seqal")
            /usr/bin/time -a -o $2/seqal_alignment_time.out seal_seqal -a $3 $4 $5 &> $2/$4.out
            shift;;

        "bwa")
            # BWA needs to do two alignments, as the reads are paired
            for fastq in `ls $3`
            do
                /usr/bin/time -a -o $2/bwa_alignment_time.out bwa aln -t $CPUS $4 $3/$fastq > $RESULTS_BWA_ALIGNMENTS/$fastq.sai
            done
            shift;;
    esac
}


function execute_tests_seqal() {
    # param $1: Genome name
    # param $2: DATA dir
    # param $3: RESULTS dir
    # param $4: REF dir
    techo -----------------------------------------------------------------------
    techo "Executing seal tests for $1's genome. Today is `date`"
    techo -----------------------------------------------------------------------
    for dataset in `ls $2/`
    do
        techo "Moving $dataset into HDFS..."
        timeit "transfer" $3/$dataset $2/$dataset && techo "DONE!"
        techo "Converting data from fastq to prq..."
        timeit "prq" $3/$dataset $dataset ${dataset}_prq && techo "DONE!"
        #Execute the alignment several times
        for i in `seq ${EXEC_TIMES}`
        do
            techo "Executing alignment for ${dataset}... Execution #$i"
            timeit "seqal" $3/$dataset ${dataset}_prq ${dataset}_seqal_$i $4 && techo "DONE!"
        done
    done
    techo "Seal tests for $1's genome have ended!"
}


function execute_tests_bwa() {
    # param $1: Genome name
    # param $2: DATA dir
    # param $3: RESULTS dir
    # param $4: REF fastq
    techo -----------------------------------------------------------------------
    techo "Executing bwa tests for $1's genome. Today is `date`"
    techo -----------------------------------------------------------------------
    for dataset in `ls $2/`
    do
        techo "Aligning reads in $dataset with BWA"
        timeit "bwa" $3/$dataset $2/$dataset $4
        techo "DONE!"
        techo "Seal tests for $1's genome have ended!"
    done
}


#Main program, parse the arguments (in a rudimentary way)
case "$1" in
    -e|--ecoli)
        execute_tests_seqal "Escherichia Coli" $ECOLI_DATA $RESULTS_SEAL_ECOLI $REF_ECOLI_SEQAL
        execute_tests_bwa "Escherichia Coli" $ECOLI_DATA $RESULTS_BWA_ECOLI $REF_ECOLI_BWA
        echo "Take a look at the results :-)" | mail -s "[THESIS] Seal tests for gnome Escherichia Coli ended" $MAILTO
        shift;;

    -h|--human)
        execute_tests_seqal "Human" $HG19_DATA $RESULTS_SEAL_HG19 $REF_HG19_SEQAL
        execute_tests_bwa "Human" $HG19_DATA $RESULTS_BWA_HG19 $REF_HG19_BWA
        echo "Take a look at the results :-)" | mail -s "[THESIS] Seal tests for gnome Human ended" $MAILTO
        shift;;

    -d|--dm3)
        execute_tests_seqal "Drosophila Melanogaster" $DM3_DATA $RESULTS_SEAL_DM3 $REF_DM3_SEQAL
        execute_tests_bwa "Drosophila Melanogaster" $DM3_DATA $RESULTS_BWA_DM3 $REF_DM3_BWA
        echo "Take a look at the results :-)" | mail -s "[THESIS] Seal tests for gnome Drosophila Melanogaster ended" $MAILTO
        shift;;

    *)
        echo "Option not available, please select between -e (ecoli), -h (human) or -d (dm3)"
        shift;;
esac
