"""
Several utilities to interact with the VM pool in the opennebula cluster.
"""

from __future__ import with_statement
from fabric.api import *
from fabric.operations import sudo
from IPython.utils.io import stdout

import json
from xml.etree import ElementTree as et
from subprocess import check_call as cc

from xmlToJson import XMLtoJSON

env.user = 'ubuntu'
env.password = 'ubuntu'


def help():
    h = """
    Usage:

    fab -f utils.py {function}[:target=<vm_target_name>]
    
    functions = remove_data_folder
                restart_hadoop_services
                clean_logs_and_restart
    targets   = datanode
                namenode
                
    Ex:
        fab -f utils.py test:target=namenode
    """
    print h
    

def _get_ips_list(target='datanode'):
    """Return a list of IPs corresponding to the VMs found with
    name target.
    
    :target: name of the VM to get the IP from
    """
    ips = []
    dn_list = ['onevm', 'list', '-x']
    with open('/tmp/vm_list.xml', 'w') as f:
        cc(dn_list, stdout=f)
    p = XMLtoJSON(input='/tmp/vm_list.xml')
    vms = json.loads(p.parse())
    for vm in vms['VM_POOL']['VM']:
        if vm['NAME'] == target:
            ips.append(vm['TEMPLATE']['NIC']['IP'])
    return ips


@parallel
def _remove_data_folder():
    cmd = 'sudo rm -rf /hdfs/data/{n}/*'
    sudo(cmd.format(n='1'))
    sudo(cmd.format(n='2'))


@parallel
def _restart_hadoop_services():
    sudo('for s in /etc/init.d/hadoop-*; do $s restart; done')
    

@parallel
def _clean_logs_and_restart():
    sudo('rm -rf /var/log/hadoop-hdfs/* && rm -rf /var/log/hadoop-0.20-mapreduce/*')
    sudo('for s in /etc/init.d/hadoop-*; do $s restart; done')


def remove_data_folder(target='datanode'):
    """Removes the temporal hdfs data folder from all target VMs
    """
    ips = _get_ips_list(target)
    execute(_remove_data_folder, hosts=ips)
    

def restart_hadoop_services(target='datanode'):
    """Restart hadoop services in all target VMs
    """
    ips = _get_ips_list(target)
    execute(_restart_hadoop_services, hosts=ips)


def clean_logs_and_restart(target='datanode'):
    """Removes all logs from Hadoop and HDFS (they can become very huge)
    """
    ips = _get_ips_list(target)
    execute(_clean_logs_and_restart, hosts=ips)


## TEST FUNCTION ##

@parallel
def _test():
    run('hostname')


def test(target='datanode'):
    """Test the connection to target VMs
    """
    ips = _get_ips_list(target)
    print '{n} target VM(s) with name {name}'.format(n=str(len(ips)), name=target)
    execute(_test, hosts=ips)