from timeit import Timer
import os

read = """ CRESSIA_242:1:2204:1453;{readID}#0\tNTTAATAAGAATGTCTGTTGTGGCTTAAAA\t#<<<8><:<;DDDDDDDDD=DDDBD@@@@@\tNNGTAAAACCCATATATTGAAAACTACAAA\t#8658D9799DDDD@DDDDDDDDDD@DDDD"""

def _generate_dummy_prq(fname, size):
    """ Generates random reads with dummy qualities
    """

    if not os.path.exists(fname):
        n = 0
        with open(fname, 'w') as f:
            while os.path.getsize(fname) <= size*1024.*1024.:
                # Identify reads uniquely
                f.write(read.format(readID=str(n)) + '\n')
                n += 1
    else:
        print 'A file named test.prq already exists in this directory, please \
               move or delete it'

os.mkdir('test_data')
os.chdir('test_data')

for i in range(3):
    size = 2**i
    os.mkdir(str(size))
    print 'Generating .prq file of ' + str(size) + 'MB...'
    t = Timer(lambda: _generate_dummy_prq(str(size) + '/test_' + str(size) + 'MB.prq', size))
    s = 'Took ' + str(t.timeit(number=1)) + ' seconds'
    print s + '\n'