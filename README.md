# Massive parallelization of bcbio-nextgen pipeline using MapReduce/Hadoop

This is the repository in which to store all the files related with my Ms.Thesis (particularily the report).

## Directory structure
* **papers**: Papers read/to read
* **tex**: Latex files with the report of the thesis
